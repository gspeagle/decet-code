# Requirements
 - opam
 - OCaml
 - jbuilder
 - postgreSQL

# Installing and Configuring PostgreSQL
Update software packages and install postgresql
```sh
$ sudo apt-get update
$ sudo apt-get upgrade
$ sudo apt-get install postgresql
```
Login as the default user.
```sh
$ sudo -u postgres psql
```
At psql prompt, change default user password.
```sh
ALTER USER postgres with encrypted password 'my_password';
```
Restart Postgres service.
```sh
$ sudo /etc/init.d/postgresql restart
```
Configure local user connections to require login/password (replace $VERSION with your Postgres version).
```sh
$ sudo gedit /etc/postgresql/$VERSION/main/pg_hba.conf
```
Change from:
```sh
# "local" is for Unix domain socket connections only
local   all             all                                     peer
```
To:
```sh
# "local" is for Unix domain socket connections only
local   all             all                                     md5
```
Create tezosDb.
```sh
$ sudo -u postgres createdb tezosDb
```
Login as default user again and run \l to make sure your database was created.
```sh
                                  List of databases
   Name    |  Owner   | Encoding |   Collate   |    Ctype    |   Access privileges   
-----------+----------+----------+-------------+-------------+-----------------------
 postgres  | postgres | UTF8     | en_US.UTF-8 | en_US.UTF-8 | 
 template0 | postgres | UTF8     | en_US.UTF-8 | en_US.UTF-8 | =c/postgres          +
           |          |          |             |             | postgres=CTc/postgres
 template1 | postgres | UTF8     | en_US.UTF-8 | en_US.UTF-8 | =c/postgres          +
           |          |          |             |             | postgres=CTc/postgres
 tezosDb   | postgres | UTF8     | en_US.UTF-8 | en_US.UTF-8 | 

```
# Installation
```sh
$ opam pin add -yn tezosDb .
$ opam install --deps-only tezosDb
```
#### Configuration
DB connection URL is located in ./lib/dbConfig:
```
postgresql://postgres:postgres@localhost:5432/tezosDb
```
# Run
Run migrations to create tables
```sh
$ jbuilder exec migration
```
Run examples of queries to "persons" table with
```sh
$ jbuilder exec personRepository
```
Run rollback to drop tables
```sh
$ jbuilder exec rollback
```

# Documentation
> All examples for ```persons``` table.


 ./lib folder has the main functions for db manipulation.

queries.ml has the following functions:
```ocaml
val select : 'a Caqti_type.t -> string -> ('a -> 'b list -> 'b list) -> 'b list
val insert : 'a Caqti_type.t -> string -> 'a -> unit
val delete : string -> int -> unit
val truncuteTable : string -> unit
```
---
### Select
The ```select``` function accepts 3 arguments: ```tableType``` ```tableName``` ```dataAccumulator```
* ```tableType``` is [Caqti_type](http://paurkedal.github.io/ocaml-caqti/caqti/Caqti_type/index.html#row_types). Data representation:
```ocaml
type person = {
  id: int;
  firstName: string;
  surname: string;
  department: string;
  cryptoId: string;
  active:  bool;
  logins: string;
  caseFiles: string;
};;
let tableType = Caqti_type.(tup2 (tup4 int string string string) (tup4 string bool string string));;
```
* ```tableName``` = a string that represents the table name.
```ocaml
let tableName = "persons";;
```
* ```dataAccumulator``` = a function that converts database tuples to an OCaml list. This function accepts the same expression as represented by ```tableType``` :
```ocaml
let dataAccumulator =
  fun ((id, firstName, surname, department), (cryptoId, active, logins, caseFiles)) acc ->
    {id; firstName; surname; department; cryptoId; active; logins; caseFiles} :: acc;;
```

* ```selectQuery``` example without dataAccumulator:
```ocaml
let selectQuery = "SELECT FROM persons WHERE department = 'Theft and Fraud'";;
```
This function searches the ```persons``` table for the 'Theft and Fraud' department.

---
### Insert
The ```insert``` function accepts 3 arguments: ```insertType``` ```query``` ```content```
* ```insertType``` is [Caqti_type](http://paurkedal.github.io/ocaml-caqti/caqti/Caqti_type/index.html#row_types). Data representation:
```ocaml
let insertType = Caqti_type.(tup2 (tup3 string string string) (tup4 string bool string string));;
```
* ```insertQuery``` example:
```ocaml
let insertQuery = "INSERT INTO persons
        (First_Name, Surname, Department, crypto_id, active, logins, case_files)
        VALUES
        (?, ?, ?, ?, ?, ?, ?);";;
```
* ```content``` = data that will be inserted into the database. It should be represented by the same expression as ```insertType```:
```ocaml
let content = (("Eric", "Cartman", "DP"), ("cID", true, "2018-12-16 15:06:31", "case"));;
```
This function inserts data into the ```persons``` table.

---
### Delete
The ```delete``` function accepts 2 arguments: ```tableName``` ```id```
* ```tableName``` = a string which represents the table name.
```ocaml
let tableName = "persons";;
```
* ```id``` = an integer that corresponds to a tuple's ```id``` in the database:
```ocaml
let id = 1;;
```
* ```deleteQuery``` example:
```ocaml
let deleteQuery = "DELETE FROM persons WHERE ID = 1";;
```
This function deletes a tuple from the ```persons``` table with an ID of 1.

---
### Truncate
The ```truncateTable``` function accepts 1 argument: ```tableName```
* ```tableName``` = a string which represents the table name.
```ocaml
let tableName = "persons";;
```
* ```truncateQuery``` example:
```ocaml
let truncateQuery = "TRUNCATE TABLE persons";;
```
This function truncates all the tables in the database with the name ```persons```.

---
### Run queries
queriesRunner.ml has follow functions:
```ocaml
val run : string array -> unit
```
The ```run``` funtion accepts 1 argument: ```queries```
* ```queries``` = an array of strings. Each string should be a SQL statement.
```ocaml
let createPersonsTable =
"CREATE TABLE public.persons
(
  ID SERIAL PRIMARY KEY,
  First_Name character varying,
  Surname character varying,
  Department character varying,
  crypto_id  character varying,
  active boolean,
  logins timestamp,
  case_files character varying
)
WITH (
  OIDS=FALSE
);"
let dropPersonsTable = "DROP TABLE IF EXISTS public.persons;";;

let queries = [|createPersonsTable; dropPersonsTable|];;
```
The function contains Migrations-related helper functions. It runs an array of SQL queries.

