let connection_url = FileReader.read "./lib/dbConfig"

(* This is the connection pool we will use for executing DB operations. *)
let getPool =
  match Caqti_lwt.connect_pool ~max_size:10 (Uri.of_string connection_url) with
  | Ok pool -> pool
  | Error err -> failwith (Caqti_error.show err)