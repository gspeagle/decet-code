type todo = {
  id: int;
  content: string;
}

type error =
  | Database_error of string

(* Core functions *)
val select : 'a Caqti_type.t -> string -> ('a -> 'b list -> 'b list) -> 'b list
val insert : 'a Caqti_type.t -> string -> 'a -> unit
val delete : string -> int -> unit
val truncateTable : string -> unit