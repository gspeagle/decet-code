open Lib

let creteTableScripts = [|
MigrationScripts.createPersonsTable;
MigrationScripts.createLocationTable;
MigrationScripts.createEvidencPiecesTable;
MigrationScripts.createCaseFileTable;
MigrationScripts.createBlockchainInformationTable;
|]

let () = QueriesRunner.run creteTableScripts