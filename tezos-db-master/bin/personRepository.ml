open Lib

type person = {
  id: int;
  firstName: string;
  surname: string;
  department: string;
  cryptoId: string;
  active:  bool;
  logins: string;
  caseFiles: string;
}

let selectFromPersons () = 
  let tableType = Caqti_type.(tup2 (tup4 int string string string) (tup4 string bool string string)) in
  let accumulator = 
    fun ((id, firstName, surname, department), (cryptoId, active, logins, caseFiles)) acc ->
      {id; firstName; surname; department; cryptoId; active; logins; caseFiles} :: acc;
  in
  let list = Queries.select tableType "persons" accumulator in
  print_endline ("Was found " ^ (string_of_int (List.length list)) ^ " tuples:");
  List.iter (fun person -> print_endline (person.firstName ^ " " ^ person.surname)) list

let insertType = Caqti_type.(tup2 (tup3 string string string) (tup4 string bool string string))
let insertQuery = "INSERT INTO PERSONS 
        (First_Name, Surname, Department, crypto_id, active, logins, case_files) 
        VALUES 
        (?, ?, ?, ?, ?, ?, ?);"
        
let insertToPersons person = Queries.insert insertType insertQuery person
  
let truncateFromPerson () = Queries.truncuteTable "persons"
let deleteFromPerson id = Queries.delete "persons" id
  
let () = 
  selectFromPersons ();
  print_endline ""; 
  insertToPersons (("Eric", "Cartman", "DP"), ("cID", true, "2018-12-16 15:06:31", "case"));
  print_endline "";
  insertToPersons (("Kenny", "McCormick", "DP"), ("cID", true, "2018-12-16 15:06:31", "case"));
  print_endline "";
  insertToPersons (("Kyle", "Barflowski", "DP"), ("cID", true, "2018-12-16 15:06:31", "case"));
  print_endline "";
  insertToPersons (("Stan", "Marsh", "DP"), ("cID", true, "2018-12-16 15:06:31", "case"));
  print_endline "";
  selectFromPersons ();
  print_endline "";
  deleteFromPerson 1;
  print_endline "";
  selectFromPersons();
  print_endline "";
  truncateFromPerson ();
  print_endline "";
  selectFromPersons();