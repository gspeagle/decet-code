const express = require('express');
const router = express.Router();
const {eztz} = require('../eztz-library')

const queryBegin = '/:chainId([a-zA-Z0-9]+)/blocks/:block_id([a-zA-Z0-9]+)/votes/:queryEnd(*)'
const queryEndList = [
    'ballots',
    'ballot_list',
    'current_period_kind',
    'current_quorum',
    'listings',
    'proposals',
    'current_proposal',
]

router.get(queryBegin, async (req, res, next) => {
    const {chainId, block_id, queryEnd} = req.params

    if (!queryEndList.includes(queryEnd)) {
        return next()
    }

    const result = await eztz.node.query(`/${chainId}/blocks/${block_id}/votes/${queryEnd}`)
        .catch(e => {
            console.error(e)
            return e
        });

    res.send({result});
});

module.exports = router;
