Instructions for Raspberry Pi 3 Tezos node build
Start by installing Raspbian on an SD card. The NOOBS system from raspberrypi.org is the easiest and can be copy and pasted onto a freshly formatted SD card. Once Raspbian is installed follow the build instructions supplied in the Tezos Developer Documentation with two distinctions.
The supplied URL for the OPAM Package Manager is the path to the x86_amd64 architecture, but the Raspberry Pi will require the arm-hf release found here:
https://github.com/ocaml/opam/releases/download/2.0.2/opam-2.0.2-armhf-linux
Secondly, the Raspberry Pi will require that the node be built in a way that the data is stored on an external SSD.
To accomplish this, after you generate an Identity, copy the entire tezos file from your home directory into the empty external SSD.
cd into the external and run the node from the tezos directory there, and all the data will be stored externally.